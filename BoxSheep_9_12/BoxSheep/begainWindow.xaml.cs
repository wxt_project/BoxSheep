﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BoxSheep.controls;
using GameDataHelper;

namespace BoxSheep
{
    /// <summary>
    /// UserControl1.xaml 的交互逻辑
    /// </summary>
    public partial class begainWindow : UserControl
    {  
        int selectedLevel=1;
        public begainWindow()
        {
            InitializeComponent();
         
            int begainX = 40;
            int begainY = 80;
            selectedLevel = 1;
            label1.Content = "选择关卡为:"+selectedLevel;
            for (int i = 0; i < GameStateManagy.levels.Count; i++)
            {
             int id= GameStateManagy.levels[i].ID;
                string name = GameStateManagy.levels[i].name;
                levelIcon icon=new levelIcon(id,name);
                canvas1.Children.Add(icon);
                icon.RenderTransform=new TranslateTransform()
                                         {
                                             X=begainX,
                                             Y =begainY
                                         };
                begainX += 100;
                if (begainX >= 730)
                {
                    begainY += 60;
                    begainX = 40;
                }
                icon.MouseDown += new MouseButtonEventHandler(icon_MouseDown);
                icon.button1.Click+=new RoutedEventHandler(buttonicon_Click);
                }
        }

        void icon_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var myicon = (levelIcon) sender;
            selectedLevel = myicon.level;
            label1.Content = "选择关卡为:" + myicon.level;
            setLevelPresentation(selectedLevel);
          //  GameStateManagy.StartGame(myicon.level);
        }
        private void buttonicon_Click(object sender, RoutedEventArgs e)
        {
            var myicon = (Button)sender;
            selectedLevel = int.Parse(myicon.Content.ToString());
            label1.Content = "选择关卡为:" + myicon.Content.ToString();
            setLevelPresentation(selectedLevel);
            // GameStateManagy.StartGame(int.Parse(myicon.Content.ToString()));
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
           GameStateManagy.InitializeLevels();
           GameStateManagy.StartGame(selectedLevel);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            whelp help=new whelp();
            canvas2.Children.Add(help);
            help.Width = 650;
            help.Height = 400;
        }
        private void button3_Click_1(object sender, RoutedEventArgs e)
        {
            GameStateManagy.GameExit();
        }
        public void setLevelPresentation(int id)
        {
            canvas2.Children.Clear();
           LevelPresentation mylevel=new LevelPresentation(GameStateManagy.GetLevel(id));
            canvas2.Children.Add(mylevel);
            // selected
        }
    }
}
