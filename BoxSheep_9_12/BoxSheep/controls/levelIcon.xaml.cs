﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BoxSheep.controls
{
    /// <summary>
    /// levelIcon.xaml 的交互逻辑
    /// </summary>
    public partial class levelIcon : UserControl
    {
        public int level;
        public levelIcon(int levelid,string name)
        {
            InitializeComponent();
            level = levelid;
            button1.Content = levelid.ToString();
            label1.Content = name;
        }
        public levelIcon()
        {
            InitializeComponent();
          
        }
    }
}
