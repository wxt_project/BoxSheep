﻿using System.Windows;
using System.Windows.Controls;
namespace BoxSheep.controls
{

    /// <summary>
    /// GameEnd.xaml 的交互逻辑
    /// </summary>
    public partial class GameEnd : UserControl
    {
        int level ;
        public GameEnd(int level,int timeCount,int stepCount)
        {
            this.level = level;
            int time = timeCount/1000;
            InitializeComponent();
            label1.Content = "当前关卡：第" + level.ToString() + "关\n" + "所用时间：" + (timeCount/1000).ToString() + "s\n" + "总步数:" +
                             stepCount.ToString() + "步\n"+"总分数：\n"+((100-time)*100+(100-stepCount)*100).ToString();
                         if(level>=GameStateManagy.levels.Count)
                         {
                             button1.IsEnabled = false;
                             label1.Content += "当前为最后一个关卡了";
                         }
        }
        

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            GameStateManagy.StartGame(level);
        }

        private void image1_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            GameStateManagy.StartGame(level+1);
        }
    }
}
