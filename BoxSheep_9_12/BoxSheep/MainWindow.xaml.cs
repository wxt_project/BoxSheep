﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BoxSheep
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {     
        MainGame myGame;
        public MainWindow()
        {
            InitializeComponent();
            begainWindow bw = new begainWindow();
            bw.Width = 800;
            bw.Height = 600;
            GameStateManagy.InitializeLevels();
            canvas1.Children.Add(bw);
   GameStateManagy.setMainWindow(this);

   this.MouseDown += new MouseButtonEventHandler(Window_MouseDown);
   this.Closed += new EventHandler(MainWindow_Closed);
        }

        void MainWindow_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }
       
        public void startGame(int level)
        {

            if (myGame != null)
            {
                canvas1.Children.Remove(myGame);
                this.KeyUp -= myGame.GameApp_KeyUp;
                this.KeyDown -= myGame.GameApp_KeyDown; 
                myGame = null;
                GameStateManagy.InitializeLevels();
            }

            myGame = new MainGame(level);
              canvas1.Children.Clear();
            this.KeyUp += myGame.GameApp_KeyUp;
            this.KeyDown += myGame.GameApp_KeyDown;
            canvas1.Children.Add(myGame);
        
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
    }
}
