﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameDataHelper;

namespace BoxSheep
{
public  static  class GameStateManagy
{
    public   static List<Level> levels=new List<Level>(); 
    private static MainWindow mainWindow=new MainWindow();
    public static void setMainWindow(MainWindow mw)
    {
        mainWindow = mw;
    }
    public static  void GameExit()
    {
        mainWindow.Close();
        
    }
    public static void StartGame(int level)
    {
        mainWindow.startGame(level);
    }
    public static void InitializeLevels()
    {
       levels= DataControl.GetLevelsFromXML(@"../../GameSettings/myMap.xml");
    }
    public static Level GetLevel(int id)
    {
        var thislevel = levels.Find(f=>f.ID==id);
        return thislevel;
    }
    }
}
