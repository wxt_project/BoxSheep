﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
namespace GameDataHelper
{
    /// <summary>
    /// LevelPresentation.xaml 的交互逻辑
    /// </summary>
    public partial class LevelPresentation : UserControl
    {
       // private Level myLevel;
        public LevelPresentation(Level myLevel)
        {
           // this.myLevel = myLevel;
            InitializeComponent();
            InitializePicture(myLevel);
        }
        private void InitializePicture(Level myLevel)
        {
           BitmapImage[] images=new BitmapImage[7];
           images[0] = new BitmapImage(new Uri(@"/Images/target.png", UriKind.Relative));       //目标);
            images[1] = new BitmapImage(new Uri(@"/Images/box.png", UriKind.Relative));       //箱子);
            images[2] = new BitmapImage(new Uri(@"/Images/endpicture.png", UriKind.Relative));       //终点);
            images[3] = new BitmapImage(new Uri(@"/Images/grass.png", UriKind.Relative));       //草地);
            images[4] = new BitmapImage(new Uri(@"/Images/river.png", UriKind.Relative));       //河流);
            images[5] = new BitmapImage(new Uri(@"/Images/sheep.png", UriKind.Relative));       //羊 );
            images[6] = new BitmapImage(new Uri(@"/Images/stone.png", UriKind.Relative));       //石头);
            int defaultWidth = 20;
            int defaultHeight = 20;
            Point defaultPosition = myLevel.DefaultPosition;
            int[,] mapArray = myLevel.MapArray;
            for (int i = 0; i < mapArray.GetLength(1); i++)
            {
                for (int j = 0; j < mapArray.GetLength(0); j++)
                {
                    switch (mapArray[j, i])
                    {
                        case 0:
                            break;
                        case 1:
                            Image grass = new Image();
                            grass.Source = images[3];
                            grass.Width = defaultWidth;
                            grass.Height = defaultHeight;
                            grass.RenderTransform = new TranslateTransform()
                            {
                                X = defaultPosition.X + i * defaultWidth,
                                Y = defaultPosition.Y + j * defaultHeight
                            };
                            canvas1.Children.Add(grass);
                            Canvas.SetZIndex(grass, 2);
                            break;
                        case 2:
                            Image stone = new Image();
                            stone.Source = images[6];
                            stone.Width = defaultWidth;
                            stone.Height = defaultHeight;
                            stone.RenderTransform = new TranslateTransform()
                            {
                                X = defaultPosition.X + i * defaultWidth,
                                Y = defaultPosition.Y + j * defaultHeight
                            };
                            canvas1.Children.Add(stone);
                            Canvas.SetZIndex(stone, 2);
                            //  test = stone;
                            break;
                        case 3:
                            Image river = new Image();
                            river.Source = images[4];
                            river.Width = defaultWidth;
                            river.Height = defaultHeight;
                            river.RenderTransform = new TranslateTransform()
                            {
                                X = defaultPosition.X + i * defaultWidth,
                                Y = defaultPosition.Y + j * defaultHeight
                            };
                            canvas1.Children.Add(river);
                            Canvas.SetZIndex(river, 2);
                            break;
                        default:
                            break;
                    }
                }
            }
            //箱子
            for (int i = 0; i < myLevel.Boxes.Count; i++)
            {
                Image image = new Image();

                image.Source = images[1];
                image.Width = defaultWidth;
                image.Height = defaultHeight;
                image.RenderTransform = new TranslateTransform()
                {
                    X = myLevel.Boxes[i].X * defaultWidth + defaultPosition.X,
                    Y = myLevel.Boxes[i].Y * defaultHeight + defaultPosition.Y
                };
                canvas1.Children.Add(image);
                Canvas.SetZIndex(image, 3);
            }
            //目标
            foreach (System.Drawing.Point point in myLevel.Targets)
            {
                Image targetImage = new Image();
                targetImage.Source = images[0];
                targetImage.Width = defaultWidth;
                targetImage.Height = defaultHeight;
                targetImage.RenderTransform = new TranslateTransform()
                {
                    X = point.X * defaultWidth + defaultPosition.X,
                    Y = point.Y * defaultHeight + defaultPosition.Y
                };
                canvas1.Children.Add(targetImage);
                Canvas.SetZIndex(targetImage, 3);
            }
       
            //人物
     Image       mainRole = new Image();
            //   rolePosition.X = defaultPosition.X*defaultWidth + startPosition.X*defaultWidth;
            //     rolePosition.Y = defaultPosition.Y*defaultHeight + startPosition.Y*defaultHeight;
            mainRole.Source = images[5];
            mainRole.Width = defaultWidth;
            mainRole.Height = defaultHeight;
            mainRole.RenderTransform = new TranslateTransform()
            {
                X = myLevel.StartPosition.X * defaultWidth + defaultPosition.X,
                Y = myLevel.StartPosition.Y * defaultHeight + defaultPosition.Y
            };
            canvas1.Children.Add(mainRole);
            Canvas.SetZIndex(mainRole, 5);
            //终点

        }

    }
}
