﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace GameDataHelper
{
    public class DataControl
    {
        public static string   SaveLevels(List<Level> levels)
        {
            return null;

        }
        public static List<Level> GetLevelsFromXML(string path)
        {
            XDocument document = XDocument.Load(path);
         var custs1 = document.Element("Levels");
          var custs = custs1.Element("level").Value;
          var projects = document.Descendants("level").Select(f => new Level
          {
              ID = int.Parse(f.Attribute("id").Value),
              ColuCount = int.Parse(f.Attribute("ColumnCount").Value),
              RowCount = int.Parse(f.Attribute("RowCount").Value),
              name = f.Attribute("Name").Value,
              DefaultPosition=new System.Windows.Point()
                                  {
                                      X=int.Parse(f.Attribute("defaultX").Value),
                                      Y = int.Parse(f.Attribute("defaultY").Value),
                                  },
              StartPosition = new Point()
              {
                  X = int.Parse(f.Attribute("StartX").Value),
                  Y = int.Parse(f.Attribute("StartY").Value),
              },
              ExitPosition = new Point()
              {
                  X = int.Parse(f.Attribute("EndX").Value),
                  Y = int.Parse(f.Attribute("EndY").Value),
              },
          });
          List<Level> _projects = new List<Level>();
          _projects.AddRange(projects);


            foreach (Level level in _projects)
            {
                var mylevel= document.Descendants("level").Single(f => int.Parse(f.Attribute("id").Value)== level.ID);
                string mapData = mylevel.Element("MapArray").Value;
                var targetsElements = mylevel.Element("TargetList").Elements("Target");
                var boxElements = mylevel.Element("BoxList").Elements("Box");
                int clumnCount = level.ColuCount;
                int rowCount = level.RowCount;
                string[] mapDatas = mapData.Split(',');
                  level.MapArray=new int[rowCount,clumnCount];  
                for (int i = 0; i < rowCount; i++)
                {
                    for (int j = 0; j < clumnCount; j++)
                    {
                        level.MapArray[i, j] = int.Parse(mapDatas[i*clumnCount+j]);
                    }
                }
                foreach (XElement targetsElement in targetsElements)
                {
                    int x1 = int.Parse(targetsElement.Attribute("x").Value);
                    int y1 = int.Parse(targetsElement.Attribute("y").Value);
                    Point target=new Point(x1,y1);
                    level.Targets.Add(target);
                }
                foreach (XElement boxElement in boxElements)
                {
                    int x1 = int.Parse(boxElement.Attribute("x").Value);
                    int y1 = int.Parse(boxElement.Attribute("Y").Value);
                    Point box = new Point(x1, y1);
                    level.Boxes.Add(box);
                }
            }
          return _projects;
        }  
        public static StringBuilder GetStringFromLevels(List<Level> levels )
        {
            StringBuilder text=new StringBuilder();
            text.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r");
            text.Append("<Levels>\n");
            foreach (Level level in levels)
            {
                text.Append(string.Format("<level id=\"{0}\" RowCount=\"{1}\" ColumnCount=\"{2}\" StartX=\"{3}\" StartY=\"{4}\" defaultX=\"{5}\" defaultY=\"{6}\" EndX=\"{7}\" EndY=\"{8}\" Name=\"{9}\">",
                    level.ID,level.RowCount,level.ColuCount,level.StartPosition.X,level.StartPosition.Y,level.DefaultPosition.X,level.DefaultPosition.Y,level.ExitPosition.X,level.ExitPosition.Y,level.name));
                text.Append("<BoxList>\n");
                foreach (Point point in level.Boxes)
                {
                    text.Append(string.Format( "<Box x=\"{0}\" Y=\"{1}\"\n />",point.X,point.Y));
                }
                text.Append("</BoxList>\n");

                text.Append("<TargetList>\n");
                foreach (Point target in level.Targets)
                {
                    text.Append(string.Format("<Target x=\"{0}\" y=\"{1}\" \n />", target.X, target.Y));
             
                }
                text.Append("</TargetList>\n");

                text.Append("<MapArray>\n");
                text.Append(GetMapArray(level.MapArray));
                text.Append("</MapArray>\n</level>\n");

            }
            text.Append("</Levels>");
            return text;
        }
        public static string GetMapArray(int[,] mapArray)
        {
            string mapArra = "";

            for (int i = 0; i < mapArray.GetLength(1); i++)
            {
                for (int j = 0; j < mapArray.GetLength(0); j++)
                {
                    mapArra += mapArray[i, j].ToString()+",";
                }
            }
            mapArra = mapArra.Substring(0, mapArra.Length - 1);
            return mapArra;
        }
    }
}
