﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace GameDataHelper
{
    public class Level
    {
        public int ID { set; get; } //关卡序号
        public string name { set; get; }          //关卡名称 
        public int ColuCount = 0, RowCount = 0;        //格子总行数，总列数
        public System.Windows.Point DefaultPosition;             //默认开始画格子位置
      public int[,] MapArray;                            //地图数据

      public List<Point> Targets = new List<Point>();          //目标格子位置
      public List<Point> Boxes = new List<Point>();            //箱子摆放位置
      public Point StartPosition = new Point(0, 0);         //角色所在位置
      public Point ExitPosition = new Point(0, 0);          //出口所在位置
        public Level(int index,string name)
        {
            this.ID = index;
            this.name = name;
        }

        public Level()
        {
            // TODO: Complete member initialization
        }
    }
}
