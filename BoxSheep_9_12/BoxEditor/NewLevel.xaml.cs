﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BoxEditor
{
    /// <summary>
    /// NewLevel.xaml 的交互逻辑
    /// </summary>
    public partial class NewLevel : Window
    {
        private MainWindow window;
        public NewLevel(MainWindow window )
        {
            this.window = window;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string ,string> myString=new Dictionary<string, string>();
            myString.Add("id",tb_id.Text);
            myString.Add("name",tb_Name.Text);
            myString.Add("rowCount",textBox1.Text);
            myString.Add("ColumnCount",textBox2.Text);
            window.AddNewLevel(myString);
        }
    }
}
